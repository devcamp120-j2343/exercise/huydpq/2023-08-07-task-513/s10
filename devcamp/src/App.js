import { gDevcampReact } from './data';
import devcamp from './assets/images/devcampreact.png'

function App() {
  return (
    <div>
      <h1>{gDevcampReact.title}</h1>

     <img src={gDevcampReact.image} width={500} alt='DevcampReact'/>

     <p>Tỷ lệ học sinh đang học {gDevcampReact.student()} %</p>

     <ul>
        {
          gDevcampReact.benefits.map((element, index) => {
            return <li key={index}>{element}</li>
          })
        }
      </ul>

        <img src={devcamp}width={500} alt='devcamp'/>
    </div>
  );
}

export default App;
